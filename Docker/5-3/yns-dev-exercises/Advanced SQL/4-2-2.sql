// therapist //
INSERT INTO therapists (NAME)
VALUES ('John'),
('Arnold'),
('Robert'),
('Ervin'),
('Smith');

// daily work shifts //
INSERT INTO daily_work_shifts (therapist_id, target_date, start_time, end_time)
VALUES ('1', NOW(), '14:00:00', '15:00:00'),
('2', NOW(), '22:00:00', '23:00:00'),
('3', NOW(), '00:00:00', '00:30:00'),
('4', NOW(), '05:00:00', '05:30:00'),
('1', NOW(), '21:00:00', '21:45:00'),
('5', NOW(), '05:30:00', '05:50:00'),
('3', NOW(), '02:00:00', '02:30:00');