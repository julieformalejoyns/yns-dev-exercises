SELECT e.*,(CASE WHEN po.name = "CEO" THEN "Chief Executive Officer" WHEN po.name = "CTO" THEN "Chief Technical Officer"
WHEN po.name = "CFO" THEN "Chief Financial Officer" ELSE po.name END) AS position_name FROM employee_positions ep
INNER JOIN positions po ON (ep.position_id = po.id) INNER JOIN employees e ON (ep.employee_id = e.id);