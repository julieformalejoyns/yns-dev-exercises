SELECT a.therapist_id, a.target_date, a.start_time, a.end_time,
(CASE WHEN a.start_time <= '05:59:59' AND a.start_time >= '00:00:00' 
THEN CONCAT(DATE_ADD(a.target_date, INTERVAL 1 DAY),' ', a.start_time) 
ELSE CONCAT(a.target_date ,' ', a.start_time) END)
AS sort_start_time FROM daily_work_shifts a ORDER BY target_date, sort_start_time ASC;