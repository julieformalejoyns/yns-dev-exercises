<!DOCTYPE html>
<html>
<title>Problem Exercise 3-5</title>
<style>
    label {
        width: 120px; 
        display: inline-block; 
        text-align: right;
    }
    span.shortcut {
        font-weight: bold; 
        text-decoration: underline;
    }
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 5px 13px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }    
</style>
</head>
<body>
<body style="background-color:powderblue;">
    <h4>
        You are now logged in!
    </h4>
    <br>
        <button name="submit" class="button" onclick="logout();">Logout</button>
</body>  
</html> 


<script type="text/javascript">
    function logout() {
        window.location = '3-5-4.php'
    }
</script>