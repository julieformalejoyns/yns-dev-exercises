
CREATE TABLE `users_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) DEFAULT '',
  `mname` varchar(50) DEFAULT '',
  `lname` varchar(50) DEFAULT '',
  `age` int(2) DEFAULT NULL,
  `email` varchar(100) DEFAULT '',
  `username` varchar(100) DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `profile_pic` varchar(100) DEFAULT '',
  `datetime_log` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4


INSERT INTO users_table(fname, mname, lname, age, email, username, PASSWORD, STATUS)
VALUES ('Julie Anne', 'Cases', 'Formalejo', '23', 'julieanneformalejo.yns@gmail.com', 'julie', 'pass1234');