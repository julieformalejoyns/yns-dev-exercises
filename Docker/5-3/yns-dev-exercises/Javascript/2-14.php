<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-14</title>
    <style>
        .center{
            display: block;
            margin-left: auto;
            margin-right: auto;
        } 
        .button {
            background-color: #00FF00;
            border: none;
            color: white;
            padding: 8px 20px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            margin: 4px 2px;
            display: inline-block;
            cursor: pointer;
        }
        .button1 {
            background-color: #008CBA;
        } 
        .button2 {
            background-color: #f44336;
        } 
    </style>

        <body style="background-color:powderblue;">
            <p>
                <img src="BTS1.jpg" id="sampleimg" width="450" height="300" style="border: 1px solid black;">
                <br>
                <select id="bandgroups" onchange="displayselectedimg(this);">
                    <option value="BTS1.jpg">-- Select --</option>
                        <option value="OneDirection.jpg">One Direction</option>
                        <option value="5SOS.jpg">5 Seconds of Summer</option>
                        <option value="Blackpink.jpg">Blackpink</option>
                        <option value="Twice.jpg">Twice</option>
                </select>
            </p>
        </body>  

        <script type="text/javascript">
            function displayselectedimg(elem) {
                var image = document.getElementById("sampleimg");
                image.src = elem.value;        
            }
        </script>
</html>