<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-13</title>
    <style>
        .center{
            display: block;
            margin-left: auto;
            margin-right: auto;
        } 
        .button {
            background-color: #00FF00;
            border: none;
            color: white;
            padding: 8px 20px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            margin: 4px 2px;
            display: inline-block;
            cursor: pointer;
        }
        .button1 {
            background-color: #008CBA;
        } 
        .button2 {
            background-color: #f44336;
        } 
    </style>

        <body style="background-color:powderblue;">
            <p>
                <img src="Rose1.jpg" id="sampleimg" width="300" height="550" style="border: 1px solid black;">
                <br>
                <button name="submit2" class="button button2" style="border: 1px solid black;" onclick="smallimage();">Small</button>
                <button name="submit3" class="button button3" style="border: 1px solid black;" onclick="mediumimage();">Medium</button>  
                <button name="submit1" class="button button1" style="border: 1px solid black;"  onclick="largeimage();">Large</button>
            </p>
        </body>  

        <script type="text/javascript">
            function smallimage() {
                document.getElementById("sampleimg").height="200";
                document.getElementById("sampleimg").width="300";
            }

            function mediumimage() {
                document.getElementById("sampleimg").height="400";
                document.getElementById("sampleimg").width="300";
            }

            function largeimage() {
                document.getElementById("sampleimg").height="600";
                document.getElementById("sampleimg").width="300";
            }
        </script>
</html>