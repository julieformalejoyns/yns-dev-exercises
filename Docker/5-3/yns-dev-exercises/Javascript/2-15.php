<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-15</title>
    <style>
        .dashbgcolor {
            background-color: #ce2029  !important;
        }

        .pad_rl_20 {
            padding-left: 20px;
            padding-right: 20px
        }

        .welcomeword {
            font-size: 1.7em;
            color: #FFFFFF;
            font-weight: bold;
        }

        .welcometime {
            font-size: 1.3em
            color:  #FFFFFF;
            font-weight: bold;
        }

        .welcomebox {
            height: 11vh
        }
    </style>

        <body style="background-color:powderblue;">
            <div class="card card-inverse card-info dashbgcolor" style="border-radius: 10px; box-shadow: 0 4px 8px;">
                <div class="box welcomebox">
                    <p class="pad_rl_20">
                        <span class="font-light text-white float-left welcomeword">Hi, Julie Anne C. Formalejo. Welcome Back!</span><br>
                        <span class="font-light text-white float-right welcometime" ><span id="txtdatetime"></span></span>
                    </p>
                </div>
            </div>
        </body>  

        <script type="text/javascript">
            $(function(){
                datetime();
            })

            function datetime() {
                date = new Date(currentdatetime());
                year = date.getFullYear();
                month = date.getMonth();
                months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                d = date.getDate();
                day = date.getDay();
                days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                h = date.getHours();
                ampm = h >= 12 ? 'PM' : 'AM';
                if(h<10) {
                    h = "0"+h;
                }
                m = date.getMinutes();
                if(m<10){
                    m = "0"+m;
                }
                s = date.getSeconds();
                if(s<10){
                    s = "0"+s;
                }
                result = ''+days[day]+', '+months[month]+' '+d+' '+year+' &nbsp;&nbsp;&nbsp;'+h+':'+m+':'+s + ' '+ampm+' ';

                $("#txtdatetime").html(result);
                setTimeout(function(){ datetime(); }, 1000);
                return true;
            }

            function currentdatetime() {
                var vals = "";

                $.ajax({
                    type: 'POST',
                    url: '2-15-1.php', 
                    data: 'form=currentdatetime',
                    async:false,
                    beforeSend:function(){    
               
                },
                success : function(data){
                    vals = data;
                }
            });
            return vals;
        }
        </script>
</html>