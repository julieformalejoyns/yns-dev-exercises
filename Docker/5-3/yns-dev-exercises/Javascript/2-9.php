<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-9</title>
    <style>
        .button {
            border: none;
            color: white;
            padding: 8px 20px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            margin: 0 auto;
            display: inline-block;
            cursor: pointer;
        }
        .button1 {
            background-color: #008CBA;
        } 
        .button2 {
            background-color: #f44336;
        } 
        .button3 {
            background-color: #FFFF00; color: black;
        } 
        .button4 {
            background-color: #FFA500;
        } 
        .button5 {
            background-color: #00FF00; color: black;
        } 
        .button6 {
            background-color: #8F00FF;
        } 
    </style>
        <body style="background-color:powderblue;">
            <h4 id="primary">Primary Colors</h4>
                <button name="submit2" class="button button2" onclick="document.body.style.backgroundColor = '#FF0000'; displaywhitetext();" style="border: 1px solid black; border-radius: 5%;">Red</button>
                <button name="submit3" class="button button3" onclick="document.body.style.backgroundColor = '#FFFF00'; displayblacktext();" style="border: 1px solid black; border-radius: 5%;">Yellow</button>  
                <button name="submit1" class="button button1" onclick="document.body.style.backgroundColor = '#0000FF'; displaywhitetext();" style="border: 1px solid black; border-radius: 5%;">Blue</button>

            <h4 id="secondary">Secondary Colors</h4>
                <button name="submit" class="button button4" onclick="document.body.style.backgroundColor = '#FFA500'; displaywhitetext();" style="border: 1px solid black; border-radius: 5%;">Orange</button>
                <button name="submit" class="button button5" onclick="document.body.style.backgroundColor = '#00FF00'; displayblacktext();" style="border: 1px solid black; border-radius: 5%;">Green</button>
                <button name="submit" class="button button6" onclick="document.body.style.backgroundColor = '#8F00FF'; displaywhitetext();" style="border: 1px solid black; border-radius: 5%;">Violet</button>
        </body>  

        <h3 id="text">Blackpink In Your Area!</h3>

        <script type="text/javascript">
            function displaywhitetext() {
                document.getElementById("primary").style.color="white";
                document.getElementById("text").style.color="white";
                document.getElementById("secondary").style.color="white";
            }


            function displayblacktext() {
                document.getElementById("primary").style.color="black";
                document.getElementById("secondary").style.color="black";
                document.getElementById("text").style.color="black";
            }
        </script>
</html>



