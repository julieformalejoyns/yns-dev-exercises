<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-4</title>
<style>
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 4px 8px;
        text-align: center;
        text-decoration: none;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
        margin: 0 auto;
        display: inline-block;
    }

    .button1 {
        background-color: #008CBA;
    } 
    .button2 {
        background-color: #f44336;
    } 
    .button3 {
        background-color: #e7e7e7; color: black;
    } 
</style>
<body style="background-color:powderblue;">
    
    <p>
        <input type="text" id="firstvalue" name="firstvalue" placeholder="Enter Number">
        <button name="submit" class="button" onclick="displayprimenumber();" id="primenumber">Submit</button>
        <div id="selectedprime"></div>
    </p>


    <script type="text/javascript">
        function displayprimenumber(){
            event.preventDefault();
            var maxValue = parseInt(document.getElementById("firstvalue").value);
            var result = [];

            for (let i = 2; i <= maxValue; i++) {
                if(i % 2 !== 0 || i == 2){
                    result.push(i);
                }
            }
            for (let j = 0; j < result.length; j++) {
                document.getElementById('selectedprime').innerHTML = `Prime numbers:${result}`;
            }  
        }
    </script>

</body>  
</html> 