<!DOCTYPE html>
<html>
<title>Problem Exercise 2-2</title>
<style>
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 8px 20px;
        text-align: center;
        text-decoration: none;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        margin: 0 auto;
        display: block;
    }

</style>
<body style="background-color:powderblue;">

    <button name="submit" class="button" onclick="displayalert();">Press Button</button>

    <script type="text/javascript">
        function displayalert() {
            var x;
            var y = confirm("Proceed to next page?");
            if (y == true) {
                x = "Success proceed!";
                window.location.href = "2-2-1.php";
            }
            else {
                x = "You pressed Cancel!";
            }
            document.getElementById("demo").innerHTML = x;
        }
    </script>
</body>  
</html> 