<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-5</title>
<style>
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 8px 15px;
        text-align: center;
        text-decoration: none;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
        margin: 0 auto;
        display: inline-block;
    }

    .button2 {
        background-color: #f44336;
    }

</style>
<body style="background-color:powderblue;">
    
    <p>
        <button name="submit" class="button" onclick="appendlabel();" id="primenumber">Add</button>
        <br><br>
        <div id="displaylabel">
        	
        </div>
    </p>

    <script type="text/javascript">
 		function appendlabel() {
			var x=1;
        	var num = x++;
        	$('#displaylabel').append("<span class='label success' id='removeselectedlbl"+ num +"''>YNS Philippines&nbsp</span><button name='submit' class='button button2' onclick='removelabel("+ num +");' id='removeselectedlblbtn"+ num +"'>Remove</button>&nbsp;");
		}

		function removelabel(id){
        	$("#removeselectedlbl"+id).remove();
        	$("#removeselectedlblbtn"+id).remove();
    	} 

    </script>
</body>  
</html>
