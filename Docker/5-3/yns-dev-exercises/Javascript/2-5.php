<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-5</title>
<style>
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 4px 8px;
        text-align: center;
        text-decoration: none;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
        margin: 0 auto;
        display: inline-block;
    }

    .label {
        color: black;
        padding: 8px;
        font-family: Arial;
    }

</style>
<body style="background-color:powderblue;">
    
    <p>
        <h5>Enter Character</h5>
        <input type="text" id="selectedcharacter" name="selectedcharacter" placeholder="">  
        <button name="submit" class="button" onclick="displaytext();" id="primenumber">Submit</button>
        <br><br>
        <span class="label success" id="displaycharacters"></span>
    </p>

    <script type="text/javascript">
        function displaytext() {
            //Reference the TextBox.
            var txtName = document.getElementById("selectedcharacter");
 
            //Reference the Label.
            var lblName = document.getElementById("displaycharacters");
 
            //Copy the TextBox value to Label.
            lblName.innerHTML = txtName.value;
        }
    </script>

</body>  
</html> 

