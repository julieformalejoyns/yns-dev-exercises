<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-3</title>
<style>
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 4px 8px;
        text-align: center;
        text-decoration: none;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
        margin: 0 auto;
        display: inline-block;
    }

    .button1 {
        background-color: #008CBA;
    } 
    .button2 {
        background-color: #f44336;
    } 
    .button3 {
        background-color: #e7e7e7; color: black;
    } 
</style>
<body style="background-color:powderblue;">
    
    <p>
        <input type="text" id="firstvalue" name="firstvalue" placeholder="First Number">
        <select id="arithmetic" onchange="displayarithmetic();">
            <option>-- Select --</option>
            <option value="Add">Add</option>
            <option value="Subtract">Subtract</option>
            <option value="Multiply">Multiply</option>
            <option value="Divide">Divide</option>
        </select>
        <input type="text" id="secondvalue" name="secondvalue" placeholder="Second Number">    
        <button name="submit" class="button" onclick="additon();" id="addition" style="display: none;">Submit</button>
        <button name="submit" class="button button1" onclick="subtraction();" id="subtraction" style="display: none;">Submit</button>
        <button name="submit" class="button button2" onclick="multiplication();" id="multiplication" style="display: none;">Submit</button>
        <button name="submit" class="button button3" onclick="division();" id="division" style="display: none;">Submit</button>
        <input type="text" id="answer" name="answer" />   
    </p>


    <script type="text/javascript">
        function displayarithmetic() {
            var arithmetic = document.getElementById("arithmetic");
            var selectedType = arithmetic.options[arithmetic.selectedIndex].value;
            if (selectedType=="Add") {
                $("#addition").show();
                $("#subtraction").hide();
                $("#multiplication").hide();
                $("#division").hide();
                $('#firstvalue').val("");  
                $('#secondvalue').val("");  
                $('#answer').val("");  
            } else if (selectedType=="Subtract") {
                $("#subtraction").show();
                $("#addition").hide();
                $("#multiplication").hide();
                $("#division").hide();
                $('#firstvalue').val("");  
                $('#secondvalue').val("");  
                $('#answer').val("");   
            } else if (selectedType=="Multiply") {
                $("#multiplication").show();
                $("#addition").hide();
                $("#subtraction").hide();
                $("#division").hide();
                $('#firstvalue').val("");  
                $('#secondvalue').val("");  
                $('#answer').val("");  
            } else if (selectedType=="Divide") {
                $("#division").show();
                $("#addition").hide();
                $("#subtraction").hide();
                $("#multiplication").hide();
                $('#firstvalue').val("");  
                $('#secondvalue').val("");  
                $('#answer').val("");  
            } else {
                $("#addition").hide();
                $("#subtraction").hide();
                $("#multiplication").hide();
                $("#division").hide();
                $('#firstvalue').val("");  
                $('#secondvalue').val("");  
                $('#answer').val("");  
            }
        }

        function additon() {
            var a, b, c;
            a = Number(document.getElementById("firstvalue").value);
            b = Number(document.getElementById("secondvalue").value);
            c = a + b;
            document.getElementById("answer").value= c;
        }

        function subtraction() {
            var a, b, c;
            a = Number(document.getElementById("firstvalue").value);
            b = Number(document.getElementById("secondvalue").value);
            c = a - b;
            document.getElementById("answer").value= c;
        }

        function multiplication() {
            var a, b, c;
            a = Number(document.getElementById("firstvalue").value);
            b = Number(document.getElementById("secondvalue").value);
            c = a * b;
            document.getElementById("answer").value= c;
        }

        function division() {
            var a, b, c;
            a = Number(document.getElementById("firstvalue").value);
            b = Number(document.getElementById("secondvalue").value);
            c = a / b;
            document.getElementById("answer").value= c;
        }
    </script>

</body>  
</html> 
