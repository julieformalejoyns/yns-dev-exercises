<!DOCTYPE html>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<html>
<title>Problem Exercise 2-11</title>
    <style>
        .button {
            background-color: #008CBA;
            border: none;
            color: white;
            padding: 8px 20px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
            margin: 4px 2px;
            display: block;
            cursor: pointer;
            margin-left :630px; 
            margin-bottom: 400px;
        } 
    </style>

        <body id="background">
            <h3>Among Us Players:</h3>
            <ol>
                <li>Disguised Toast</li>
                <li>Valkyrae</li>
                <li>Sykkuno</li>
                <li>Corpse Husband</li>
            </ol>
        </body>  

        <script type="text/javascript">
            jQuery(function ($) {
                function changeColor(selector, colors, time) {

                var curCol = 0,
                    timer = setInterval(function () {
                    if (curCol === colors.length) curCol = 0;
                        $(selector).css("background-color", colors[curCol]);
                            curCol++;
                        }, time);
                    }
                $(window).load(function () {
                    changeColor("#background", ["#0000FF", "#ADD8E6"], 3000);
                });
            });
        </script>
</html>