<!DOCTYPE html>
<html>
<title>Problem Exercise 1-6</title>
<style>
    label {
        width: 120px; 
        display: inline-block; 
        text-align: right;
    }
    span.shortcut {
        font-weight: bold; 
        text-decoration: underline;
    }
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 5px 13px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }    
</style>
</head>
<body>
<body style="background-color:powderblue;">

    <form style="width: 460px; margin: auto; border: solid 1px; padding: 0 1em;" method="post" action="1-6-1.php">
        <h2>User Information</h2>
        <p>Please enter your personal details</p>
        <p><label for="fname">First Name: </label><input type="text" id="fname" name="fname" /></p>
        <p><label for="mname">Middle Name: </label><input type="text" id="mname" name="mname" /></p>
        <p><label for="lname">Last Name: </label><input type="text" id="lname" name="lname" /></p>
        <p><label for="suf">Suffix: </label><input type="text" id="suf" name="suf" /></p>
        <p><label for="age">Age: </label><input type="number" maxlength="2" id="age" name="age" /></p>
        <p><label for="tel">Telephone: </label><input type="tel" size="15" id="phone" name="phone"></p>
        <p><label for="mobile">Mobile: </label><input type="tel" size="15" id="mobile" name="mobile"></p>
        <p><label for="emailaddr">Email Address: </label><input type="email" maxlength="100" id="emailaddr" name="emailaddr"></p>
        <p><label for="street">Street Address: </label><input type="text" maxlength="100" id="street" name="street"></p>
        <button name="submit" class="button">Submit Form</button>
    </form>

</body>  
</html> 