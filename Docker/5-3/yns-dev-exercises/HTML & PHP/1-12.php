<?php
    session_start();
    set_time_limit(0);

    $fname = $_POST['fname '] ?? '';
    $mname = $_POST['mname'] ?? '';
    $lname = $_POST['lname'] ?? '';
    $age = $_POST['age'] ?? '';
    $emailaddr = $_POST['emailaddr'] ?? '';
    $profile = $_POST['profile'] ?? '';

    $requirements = array();
    $inputdetails = array('fname','mname', 'lname', 'age','emailaddr');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($inputdetails as $key => $input) {
            if (empty($_POST[$input])){
                array_push($requirements,ucwords($input) .' is required');
            }
        }
        if (!ctype_alpha($fname) || !ctype_alpha($mname) || !ctype_alpha($lname)){
            array_push($requirements, 'Invalid name. Should not consist of numbers. Letters only');
        }
        if ($age && !is_numeric($age)){
            array_push($requirements, 'Age should be numeric');
        }
        if ($emailaddr && !filter_var($emailaddr, FILTER_VALIDATE_EMAIL)) {
            array_push($requirements, 'Invalid email address.');
        }
        if (!$requirements){
            $_SESSION['fname'] = $fname;
            $_SESSION['mname'] = $mname;
            $_SESSION['lname'] = $lname;
            $_SESSION['age'] = $age;
            $_SESSION['emailaddr'] = $emailaddr;
            $_SESSION['profile'] = $profile;
        }
    }
?>

<!DOCTYPE html>
<html>
<title>Problem Exercise 1-12</title>
<style>
    label {
        width: 120px; 
        display: inline-block; 
        text-align: right;
    }
    span.shortcut {
        font-weight: bold; 
        text-decoration: underline;
    }
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 5px 13px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }    
</style>
</head>
<body>
<body style="background-color:powderblue;">

    <?php
    $filename = "UserInformation.csv";
    if (file_exists($filename)) {
      function csvToArray($filename = '', $delimiter = ',')
      {
        if (!file_exists($filename) || !is_readable($filename))
          return FALSE;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
          while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {

            $data[] =  $row;
          }
          fclose($handle);
        }
        return $data;
      }

      $total = count(file($filename, FILE_SKIP_EMPTY_LINES));
      $limit = 10;
      $pages = ceil($total / $limit);
      $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
        'options' => array(
          'default'   => 1,
          'min_range' => 1,
        ),
      )));

      $offset = ($page - 1)  * $limit;

      $start = $offset + 1;
      $end = min(($offset + $limit), $total);

      $data = csvToArray($filename, ',');

      echo '<table>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Middle Name</th>
          <th>Email</th>
          <th>Age</th>
          <th>Profile Image</th>
        </tr>';

      for ($i = $start - 1; $i < $end; $i++) {
        echo "<tr>";
        foreach ($data[$i] as $key => $value) {

          if ($key == 6) {
            echo "<td><img src='uploads/" . $value . "' /></td>";
          } else {
            echo "<td>" . $value . "</td>";
          }
        }
        echo "</tr>";
      }
      echo '</table>';

      echo '<div class="center">';
      
      $prevlink = (true) ? '<a href="?page=1" style="color: black;">&laquo;</a> <a href="?page=' . ($page - 1) . '" style="color: black;">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
      
      $nextlink = (true) ? '<a href="?page=' . ($page + 1) . '" style="color: black;">&rsaquo;</a> <a href="?page=' . $pages . '" style="color: black;">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

      echo '<ul class="pagination">';
      echo '<li>' . $prevlink . '</li>';
      for ($counter = 1; $counter <= $pages; $counter++) {
        if ($counter == $page) {
          echo "<li><a class='active'>$counter</a></li>";
        } else {
          echo "<li><a href='?page=$counter'>$counter</a></li>";
        }
      }
      echo '<li>' . $nextlink . '</li>';
      echo '</ul>';
      echo '</div>';
    }
    ?>

</body>  
</html>
   