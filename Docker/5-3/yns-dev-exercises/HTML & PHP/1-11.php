<?php
    session_start();
    set_time_limit(0);

    $fname = $_POST['fname '] ?? '';
    $mname = $_POST['mname'] ?? '';
    $lname = $_POST['lname'] ?? '';
    $age = $_POST['age'] ?? '';
    $emailaddr = $_POST['emailaddr'] ?? '';
    $profile = $_POST['profile'] ?? '';

    $requirements = array();
    $inputdetails = array('fname','mname', 'lname', 'age','emailaddr');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($inputdetails as $key => $input) {
            if (empty($_POST[$input])){
                array_push($requirements,ucwords($input) .' is required');
            }
        }
        if (!ctype_alpha($fname) || !ctype_alpha($mname) || !ctype_alpha($lname)){
            array_push($requirements, 'Invalid name. Should not consist of numbers. Letters only');
        }
        if ($age && !is_numeric($age)){
            array_push($requirements, 'Age should be numeric');
        }
        if ($emailaddr && !filter_var($emailaddr, FILTER_VALIDATE_EMAIL)) {
            array_push($requirements, 'Invalid email address.');
        }
        if (!$requirements){
            $_SESSION['fname'] = $fname;
            $_SESSION['mname'] = $mname;
            $_SESSION['lname'] = $lname;
            $_SESSION['age'] = $age;
            $_SESSION['emailaddr'] = $emailaddr;
            $_SESSION['profile'] = $profile;
        }
    }
?>

<!DOCTYPE html>
<html>
<title>Problem Exercise 1-11</title>
<style>
    label {
        width: 120px; 
        display: inline-block; 
        text-align: right;
    }
    span.shortcut {
        font-weight: bold; 
        text-decoration: underline;
    }
    .button {
        background-color: #4CAF50; 
        border: none;
        color: white;
        padding: 5px 13px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }    
</style>
</head>
<body>
<body style="background-color:powderblue;">

    <form style="width: 460px; margin: auto; border: solid 1px; padding: 0 1em;" method="post" action="1-11.php" enctype="multipart/form-data">
        <h2>User Information</h2>
        <p>Please enter your personal details</p>
        <p><label for="fname">Picture: </label><input type="file" id="file" name="file" value="<?= $profile ?>"></p>
        <p><label for="fname">First Name: </label><input type="text" id="fname" name="fname"></p>
        <p><label for="mname">Middle Name: </label><input type="text" id="mname" name="mname"></p>
        <p><label for="lname">Last Name: </label><input type="text" id="lname" name="lname"></p>
        <p><label for="suf">Suffix: </label><input type="text" id="suf" name="suf" /></p>
        <p><label for="age">Age: </label><input type="text" minlength="1" maxlength="2" id="age" name="age" /></p>
        <p><label for="tel">Telephone: </label><input type="tel"  id="phone" name="phone"></p>
        <p><label for="mobile">Mobile: </label><input type="tel"  id="mobile" name="mobile"></p>
        <p><label for="emailaddr">Email Address: </label><input type="text" maxlength="100" id="emailaddr" name="emailaddr"></p>
        <p><label for="street">Street Address: </label><input type="text" maxlength="200" id="street" name="street"></p>
            <div class="requirements">
            <?php 
                if ($requirements){
                    foreach ($requirements as $key => $requirement) {
                        echo $requirement.'<br/>';
                    }
                } 
            ?>
            </div>
        <button name="submit" class="button">Submit Form</button>
    </form>

</body>  
</html>
   