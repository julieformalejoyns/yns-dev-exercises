<!DOCTYPE html>
<html>
	<title>Phase One</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
		html,body {font-family: "Raleway", sans-serif}
	</style>
	<body style="background-color: #E6E6FA;">

	<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  		<button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  		<span class="w3-bar-item w3-right">YNS</span>
	</div>

	<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  		<div class="w3-container w3-row">
    		<div class="w3-col s4">
      			<img src="JulieAnneFormalejo.jpg" style="width:46px">
    		</div>
    		<div class="w3-col s8 w3-bar">
      			<span>Welcome, <br><strong>Julie Anne Formalejo</strong></span><br>
    		</div>
  		</div>
  		<hr>
  		<div class="w3-container">
    		<h5>HTML & PHP</h5>
  		</div>
  		<div class="w3-bar-block">
    		<a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    		<a href="#" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-edit fa-fw"></i>  Problem Exercise 1-1</a>
  		</div>
	</nav>


	<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
	<div class="w3-main" style="margin-left:300px;margin-top:43px;">
  		<header class="w3-container" style="padding-top:22px">
    		<h5><b><i class="fa fa-code"></i> Title:</b> Show Hello World</h5>
    		<hr style="height: 1px; background-color: #ccc; border: none;">
    			<?php
					echo "<h2 style='text-align: center;'>Hello World</h2>";
				?>
  		</header>
	</div>

	<script>
		var mySidebar = document.getElementById("mySidebar");

		var overlayBg = document.getElementById("myOverlay");

		function w3_open() {
  			if (mySidebar.style.display === 'block') {
    			mySidebar.style.display = 'none';
    			overlayBg.style.display = "none";
  			} else {
    			mySidebar.style.display = 'block';
    			overlayBg.style.display = "block";
  			}
		}

		function w3_close() {
  			mySidebar.style.display = "none";
  			overlayBg.style.display = "none";
		}
	</script>

	</body>
</html>
