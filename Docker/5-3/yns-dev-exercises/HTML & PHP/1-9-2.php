<html>
<title>Problem Exercise 1-9</title>
<style> 
    #sample {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #sample td, #sample th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #sample tr:nth-child(even){background-color: #f2f2f2;}

    #sample tr:hover {background-color: #ddd;}

    #sample th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
    }
</style>
</head>
<body>
<body style="background-color:powderblue;">
    <table id="sample">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Age</th>
                <th>Email Address</th>
            </tr>
            <tr>
            <?php
                $csvFile = fopen('./UserInformation.csv', 'r');
                while (($csvData = fgetcsv($csvFile)) !== false) {
                        echo '<tr>';
                        for ($i=0; $i < 4 ; $i++) {     
                                echo '<td>' .$csvData[$i]. '</td>'; 
                        }
                        echo "</tr>\n"; 
                }
                fclose($csvFile);
            ?>
            </tr>
        </thead>
    </table>
</body>  
</html>