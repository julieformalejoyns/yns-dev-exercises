// users //
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT '',
  `username` varchar(20) DEFAULT '',
  `password` varchar(20) DEFAULT '',
  `fname` varchar(50) DEFAULT '',
  `mname` varchar(50) DEFAULT '',
  `lname` varchar(50) DEFAULT '',
  `suffix` varchar(50) DEFAULT '',
  `birthdate` date DEFAULT NULL,
  `age` varchar(2) DEFAULT '',
  `email` varchar(100) DEFAULT '',
  `datetime_log` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4


// questions //
CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` varchar(15) DEFAULT '',
  `question` varchar(200) DEFAULT '',
  `choice_a` varchar(50) DEFAULT '',
  `choice_b` varchar(50) DEFAULT '',
  `choice_c` varchar(50) DEFAULT '',
  `choice_d` varchar(50) DEFAULT '',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4


// answers //
CREATE TABLE `tbl_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` varchar(11) DEFAULT '',
  `answer` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

// scores //
CREATE TABLE `tbl_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT '',
  `user_score` int(2) DEFAULT NULL,
  `datetime_log` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

INSERT QUERIES

// users //
INSERT INTO tbl_users (user_id, username, password, fname, mname, lname, suffix, birthdate, age, email)
VALUES ('USER-0000001', 'julie', 'pass1234', 'Julie Anne', 'Cases', 'Formalejo', '', '1998-07-11', '23 yrs old', 'julieanneformalejo.yns@gmail.com');

// questions //
INSERT INTO tbl_questions (question_id, question, choice_a, choice_b, choice_c, choice_d)
VALUES 
('QST-0000001', 'Who said this immortal words “A Filipino is worth dying for” ?', 'Manuel Quezon', 'Ninoy Aquino', 'Ferdinand Marcos
', 'Jose Rizal'),
('QST-0000002', 'What filipino food that is added in the Oxford Dictionary?', 'Tinola', 'Mechado', 'Kaldereta', 'Lechon'),
('QST-0000003', 'Catriona Gray won the Miss Universe contest in what year?', '2017', '2018', '2019', '2016'),
('QST-0000004', 'Which Filipino boxer is known for his nickname "The Filipino Flash"?', 'Efren Reyes', 'Nonito Donaire', 'Manny Pacquiao', 'Donnie Nietes'),
('QST-0000005', 'Which city is known as the "Walled City"?', 'Intramuros', 'Malolos', 'Bulacan', 'Makati'),
('QST-0000006', 'Who discovered America?', 'Leonardo Da Vinci', 'Simon Bolivar', 'Christopher Columbus', 'Marco Polo'),
('QST-0000007', 'What is the language of Brazil?', 'English', 'Portugese', 'Italian', 'Brazilian'),
('QST-0000008', 'What is the monetary unit of Japan?', 'Yen', 'Libra', 'Euro', 'Peso'),
('QST-0000009', 'What is the closest planet to the sun?', 'Mars', 'Jupiter', 'Mercury', 'Earth'),
('QST-0000010', 'Lexus is a sub-brand of which car company?', 'BMW', 'Honda', 'Mazda', 'Toyota');

// answers //
INSERT INTO tbl_answers(question_id, answer)
VALUES 
('QST-0000001', 'Manuel Quezon'),
('QST-0000002', 'Lechon'),
('QST-0000003', '2018'),
('QST-0000004', 'Nonito Donaire'),
('QST-0000005', 'Intramuros'),
('QST-0000006', 'Christopher Columbus'),
('QST-0000007', 'Portugese'),
('QST-0000008', 'Yen'),
('QST-0000009', 'Mercury'),
('QST-0000010', 'Toyota');