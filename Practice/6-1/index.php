<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Phase One</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/fontawesome/css/fontawesome.css" />
        <link rel="stylesheet" href="../assets/fontawesome/css/brands.css" />
        <link rel="stylesheet" href="../assets/fontawesome/css/solid.css" />
    </head>
    <style type="text/css">
        html
        {  background-color: #F5F5F5 !important;    }

        .usertypebox
        {
            border-radius: 3px; 
            box-shadow: 0 0 2px 1px rgba(0,0,0,.12); 
            cursor: pointer;
            font-weight: bold;
            min-height: 380px; 
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .usertypebox h1
        {
            color: #FFF;
            margin-top: 10px;
        }

        .usertypebox p
        {
            color: #FFF;
            font-weight: normal;
            margin-top: 20px;
        }
    </style>
    <body>
        <div class="main-wrapper">
            <div class="row justify-content-md-center">
                <div class="col-md-12" style="margin-top: 100px;">
                    <div class="text-center">
                        <h1 style="font-size: 50px;">
                            <img src="../images/logo.jpg" alt="homepage" class="mix-blend-mode" style="width: 240px; height: 220px;"/>
                        <br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="margin-top: 50px; margin: 0 auto;" onclick="selectusertype('User');">
                            <div class="card usertypebox" style="background-color: #ce2029;">
                                <div class="card-body text-center">
                                    <i class="ace-icon fas fa-question" style="font-size: 100px; color: #FFF; margin-top: 50px;"></i>
                                    <h1>Let's Start</h1>
                                    <h3 style="color: white; font-weight: bold;">Online Quiz</h3>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div><!-- /.main-container -->

        <script src="../assets/plugins/jquery/jquery.min.js"></script>
        <script>
            function selectusertype(xval)
            {
                window.location = 'login.php?xtype=' + xval;
            }
        </script>
    </body>
</html>

<style type="text/css">
 .mix-blend-mode {
     mix-blend-mode: multiply;
  }    
</style>
