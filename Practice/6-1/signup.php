<?php
	include("connect.php");
	session_start();
	$_SESSION['usertype'] = $_REQUEST["xtype"];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Account - Online Quiz</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
	    <link rel="stylesheet" href="../assets/fontawesome/css/fontawesome.css" />
	    <link rel="stylesheet" href="../assets/fontawesome/css/brands.css" />
	    <link rel="stylesheet" href="../assets/fontawesome/css/solid.css" />
    	<link href="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	</head>
	<style type="text/css">
		html
        {  background-color: #F5F5F5 !important;    }

		.login-container{
		    border: 1px solid #CCC;
		    border-radius: 5px;
		    padding: 0;
		    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		    background-color: white;
		}

		.login-container .login-cont1{
		    background-image: url("../images/quiz.jpg");
		    background-position: center; /* Center the image */
		    background-repeat: no-repeat; /* Do not repeat the image */
		    background-size: cover; /* Resize the background image to cover the entire container */
		    height: 450px;
		    padding: 0;
		    /*margin-top: 5px;*/
		}

		.login-container .login-cont11{
		    height: 780px;
		    padding: 50px 50px 50px 50px;
		}

		.login-container .login-cont1 h1, .login-container .login-cont1 p{
		    text-align: center;
		    color: #000;
		}

		.login-container .login-cont2{
		    height: 780px;
		    background-color: #FFF;
		    /*padding: 0;*/
		}

		.login-container .login-cont2 .login-header{
		    text-align: center;
		    margin: 0px 20px 0px 20px;
		}

		.login-container .login-cont2 h5{
		    color: #666;
		}

		.login-container .login-cont2 h6{
		    text-align: center;
		    margin-top: 20px;
		}

		.login-container .login-cont2 p{
		    text-align: center;
		    margin-top: 100px;
		    cursor: pointer;
		}

		.login-container .login-cont2 .login-input{
		    padding: 20px 60px 60px 60px;
		    text-align: center;
		}


		.login-container .login-cont2 .input-group{
		    margin-top: 20px;
		}

		.login-container .login-cont2 .input-group input{
		    padding: 10px;
		}

		.login-container .login-cont2 button{
		    width: 100%;
		    border: 1px solid #CCC;
		    padding: 10px;
		    font-size: 14px;
		    margin-top: 10px;
		    color: #FFF;
		}
	</style>
	<body>
		<!-- USER SIGNUP -->
		<div class="row justify-content-md-center">
			<div class="col-md-6 col-md-offset-3">
				<div class="row login-container" style="margin-top: 130px;">
					<div class="col-sm-6 login-cont1">
						<div class="login-cont11" style="border-right: 1px solid black; width: 100%;">
							<div class="card-body text-center"> 
								<br><br><br><br><br><br><br><br>
								<br><br><br><br><br><br><br><br>
								<br><br><br><br><br><br>
                            	<h1><b>Welcome, Back!</b></h1>
                            	<p style="font-size: 18px;">To keep connected with us, please login with your personal info.</p>
								<button class="btn btn-sm" onclick="displaylogin();" style="width: 60%; border: 1px solid #ce2029; padding: 10px; font-size: 14px; margin-top: 10px; color: #000; background-color: white; border-radius: 30px;"><span>SIGN IN</span></button>
                        	</div>
						</div>
					</div>
					<div class="col-sm-6 login-cont2">
						<div class="login-header">
							<img src="../images/logo.jpg" alt="homepage" style="width: 160px; height: 160px;"/>
							<h1>Create Account</h1>
							<h5 class="light-blue">Please enter your personal details</h5>
						</div>
						<div class="row login-input">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon11" id="txtusername" autocomplete="off">
		                        		</div>
									</div>
									<br>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-lock"></i></span>
		                            		</div>
		                            		<input type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon11" id="txtpassword" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-lock"></i></span>
		                            		</div>
		                            		<input type="password" class="form-control" placeholder="Confirm Password" aria-label="EmailAddress" aria-describedby="basic-addon11" id="txtconfirmpass" autocomplete="off">
		                        		</div>
									</div>

								</div>
								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="First Name" aria-label="FirstName" aria-describedby="basic-addon11" id="txtfirstname" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Middle Name" aria-label="MiddleName" aria-describedby="basic-addon11" id="txtmiddlename" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Last Name" aria-label="LastName" aria-describedby="basic-addon11" id="txtlastname" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Suffix" aria-label="Suffix" aria-describedby="basic-addon11" id="txtsuffix" autocomplete="off">
		                        		</div>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-calendar"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control date-picker" placeholder="Birth Date" aria-label="BirthDate" aria-describedby="basic-addon11" id="txtfirstname" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Age" aria-label="Age" aria-describedby="basic-addon11" id="txtmiddlename" autocomplete="off">
		                        		</div>
									</div>
									<div class="col-md-6">
										<div class="input-group mb-6">
		                            		<div class="input-group-prepend">
		                                		<span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-email"></i></span>
		                            		</div>
		                            		<input type="text" class="form-control" placeholder="Email Address" aria-label="EmailAddress" aria-describedby="basic-addon11" id="txtmiddlename" autocomplete="off">
		                        		</div>
									</div>
								</div>
								<br>
								<button class="btn btn-sm" onclick="displaylogin();" style="background-color: #ce2029; border-radius: 30px; width: 300px ;"><span>SIGN IN</span></button>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="../assets/plugins/jquery/jquery.min.js"></script>
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    	<script src="../assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
        	$(function(){
        		$(".date-picker").datepicker({
            		autoHide: true,
            		format: 'yyyy/mm/dd',
            		todayHighlight: true
        		});
        	})

        	function displaylogin() {
        		window.location = 'login.php';
        	}
        </script>

	</body>
</html>