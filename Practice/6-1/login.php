<?php
	include("connect.php");
	session_start();
	$_SESSION['usertype'] = $_REQUEST["xtype"];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login Account - Online Quiz</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
	    <link rel="stylesheet" href="../assets/fontawesome/css/fontawesome.css" />
	    <link rel="stylesheet" href="../assets/fontawesome/css/brands.css" />
	    <link rel="stylesheet" href="../assets/fontawesome/css/solid.css" />
	</head>
	<style type="text/css">
		html
        {  background-color: #F5F5F5 !important;    }

		.login-container{
		    border: 1px solid #CCC;
		    border-radius: 5px;
		    padding: 0;
		    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
		    background-color: white;
		}

		.login-container .login-cont1{
		    background-image: url("../images/quiz.jpg");
		    background-position: center; /* Center the image */
		    background-repeat: no-repeat; /* Do not repeat the image */
		    background-size: cover; /* Resize the background image to cover the entire container */
		    height: 530px;
		    padding: 0;
		    /*margin-top: 5px;*/
		}

		.login-container .login-cont11{
		    height: 730px;
		    padding: 50px 50px 50px 50px;
		}

		.login-container .login-cont1 h1, .login-container .login-cont1 p{
		    text-align: center;
		    color: #000;
		}

		.login-container .login-cont2{
		    height: 730px;
		    background-color: #FFF;
		    /*padding: 0;*/
		}

		.login-container .login-cont2 .login-header{
		    text-align: center;
		    margin: 20px 20px 0px 20px;
		}

		.login-container .login-cont2 h5{
		    color: #666;
		}

		.login-container .login-cont2 h6{
		    text-align: center;
		    margin-top: 20px;
		}

		.login-container .login-cont2 p{
		    text-align: center;
		    margin-top: 100px;
		    cursor: pointer;
		}

		.login-container .login-cont2 .login-input{
		    padding: 20px 60px 60px 60px;
		    text-align: center;
		}


		.login-container .login-cont2 .input-group{
		    margin-top: 20px;
		}

		.login-container .login-cont2 .input-group input{
		    padding: 10px;
		}

		.login-container .login-cont2 button{
		    width: 100%;
		    border: 1px solid #CCC;
		    padding: 10px;
		    font-size: 14px;
		    margin-top: 10px;
		    color: #FFF;
		}
	</style>
	<body>
		<input type="hidden" id="txtuserglobalusertype" value ='<?=  $_SESSION['usertype']; ?>' >
		<!-- USER LOGIN -->
		<div class="row justify-content-md-center">
			<div class="col-md-6 col-md-offset-3">
				<div class="row login-container" style="margin-top: 130px;">
					<div class="col-sm-7 login-cont1">
						<div class="login-cont11" style="border-right: 1px solid black; width: 100%;">
							<div class="card-body text-center"> 
								<br><br><br><br><br><br><br><br>
								<br><br><br><br><br><br><br><br>
								<br><br><br><br><br><br><br><br><br>
                            	<h1><b>Hello, Friend!</b></h1>
                            	<p style="font-size: 18px;">Enter your personal details and start your journey with us.</p>
								<button class="btn btn-sm" onclick="displaysignup();" style="width: 60%; border: 1px solid #ce2029; padding: 10px; font-size: 14px; margin-top: 10px; color: #000; background-color: white; border-radius: 30px;"><span>SIGN UP</span></button>
                        	</div>
						</div>
					</div>
					<div class="col-sm-5 login-cont2">
						<div class="login-header">
							<img src="../images/logo.jpg" alt="homepage" style="width: 160px; height: 160px;"/>
							<h1>Online Quiz</h1>
						</div>
						<div class="row login-input">
							<div class="col-md-12">
								<h5 class="light-blue">Please login with your account</h5>
								<div class="input-group mb-3">
		                            <div class="input-group-prepend">
		                                <span class="input-group-text text-white" id="basic-addon11" style="background-color: #ce2029;"><i class="ti-user"></i></span>
		                            </div>
		                            <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon11" id="inputusername" autocomplete="off">
		                        </div>
		                        <div class="input-group mb-3">
		                            <div class="input-group-prepend">
		                                <span class="input-group-text text-white" id="basic-addon33" style="background-color: #ce2029;"><i class="ti-lock"></i></span>
		                            </div>
		                            <input type="inputpassword" class="form-control" placeholder="Password" aria-label="Password" id="txtpassword" autocomplete="off">
		                        </div>
								<button class="btn btn-sm" onclick="login();" style="background-color: #ce2029; border-radius: 30px;"><span>LOGIN</span></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="../assets/plugins/jquery/jquery.min.js"></script>
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript">
        	function login() {
        		var txtusername = $('#inputusername').val();
        		var txtpassword = $('#inputpassword').val();
        		var utype = $('#txtuserglobalusertype').val();

				if (txtusername == "") {	
					alert("Please enter username.");	
				} else if (txtpassword == "") {	
					alert("Please enter your password.");	
				} else {
        			$.ajax({
						type: 'POST',
						url: 'class.php',
						data: 'txtusername=' + txtusername + '&txtpassword=' + txtpassword + '&utype=' + utype + '&form=login',
						success: function(data){
							window.location = 'mainpage.php';
						}
        			});
        		}
        	}

        	function displaysignup() {
        		window.location = 'signup.php';
        	}
        </script>

	</body>
</html>