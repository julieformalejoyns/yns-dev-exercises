<?php 
  session_start();
  set_time_limit(0);

    $fname = $_POST['fname'] ?? '';
    $mname = $_POST['mname'] ?? '';
    $lname = $_POST['lname'] ?? '';
    $age = $_POST['age'] ?? '';
    $emailaddr = $_POST['emailaddr'] ?? '';
    $bdate = $_POST['bdate'] ?? '';
    $uname = $_POST['uname'] ?? '';
    $pass = $_POST['pass'] ?? '';
    $profile = $_SESSION['profile'] ?? '';
    $profileview = "";
    $requirements = array();
    $inputdetails = array('fname', 'mname', 'lname', 'age', 'emailaddr', 'bdate', 'uname', 'pass', 'profile');

  
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $targetDIR = 'images/';
      $filepath = $targetDIR . $_FILES["file"]["name"];
      $imageFileType = strtolower(pathinfo($filepath,PATHINFO_EXTENSION));

      foreach ($inputdetails as $key => $input) {
          if (empty($_POST[$input])){
              array_push($requirements,ucwords($input) .' is required');
          }
      }
      if ($age && !is_numeric($age)){
          array_push($requirements, 'Age should be numeric');
      }

      if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
          array_push($requirements, 'Email is not a valid email address');
      }
      
      if (!file_exists('images')){
          mkdir('images');
      }

      if ($username && !preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)) {
          array_push($requirements, 'Invalid username. Should only consist of alphanumeric characters.');;
       }

      if(!ctype_alpha($fname) || !ctype_alpha($mname) || !ctype_alpha($lname)){
          array_push($requirements, 'Invalid name. Should not consist of numbers. Letters only');
      }

      if ($imageFileType && $imageFileType != "jpg" && $imageFileType != "png") {
          array_push($requirements, 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
      }

      if (!$requirements && move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) 
      {
          $imageView = $filepath ;
      } 

      if (!$requirements){
          $_SESSION['fname'] = $fname;
          $_SESSION['mname'] = $mname;
          $_SESSION['lname'] = $lname;
          $_SESSION['age'] = $age;
          $_SESSION['emailaddr'] = $emailaddr;
          $_SESSION['uname'] = $uname;
          $_SESSION['pass'] = $pass;
          $_SESSION['profile'] = $profileview;

          $query = "INSERT INTO users_table (fname, mname, lname, age, email, username, password) 
          VALUES('$fname', '$mname', '$lname', '$age', '$emailaddr', '$uname', '$pass')";

            if ($connect->query($query) === TRUE) {
                header("location:3-5-2.php");
            } else {
                echo 'Error:' .$query. '<br>'. $connect->error;
            }
      }
  }
?>