SELECT employee.last_name AS employee, boss.last_name AS boss FROM
employees employee JOIN employees boss WHERE employee.boss_id = boss.id;