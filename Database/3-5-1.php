<?php
  $connect = mysqli_connect('localhost','root','','sampledb');

  function generateID( $field , $table , $prefix , $connect ){
    $sql = "SELECT ".$field." FROM ".$table." WHERE ".$field." LIKE '%".$prefix."%' ORDER BY ".$field." DESC LIMIT 1";
    $res = mysqli_query( $connect , $sql);
    $sqlarr = mysqli_fetch_array( $res );

    $auto = explode('-',$sqlarr[0] );

    return $prefix."-".str_pad( ( $auto[1] + 1) , 6 , '0' , STR_PAD_LEFT );
  }
?>