SELECT e.first_name, e.middle_name, e.last_name FROM employees e JOIN employee_positions ep ON (ep.employee_id = e.id) 
HAVING COUNT(ep.employee_id) > 2;