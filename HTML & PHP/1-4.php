<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
            .w3-sidebar {
            z-index: 3;
            width: 250px;
            top: 43px;
            bottom: 0;
            height: inherit;
        }

        .error, .numericerror {
            color: #FF0000;
        }
    </style>    
</head>
<body>
<?php
    // define variables and set to empty values
    $selectednoerror = "";
    $selectednumerror = "";
    $selectednumber = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["selectednumber"])) {
            $selectednoerror = "Please input number";
        } else {
            $selectednumber = test_input($_POST["selectednumber"]);
        }    

        if (is_numeric($_POST["selectednumber"])) {
            $secondnumerror = "This is a valid number";
        } else {
            $selectednumber = test_input($_POST["selectednumber"]);
            $selectednumerror = "Invalid character. Numbers only";
        }
    } 

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


?>
    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-4: Solve Fizzbuzz problem.<h4>
                <br>
                <form method="post">
                    <div class="w3-row-padding">
                        <div class="w3-third">
                            <label>Input Number</label>
                            <input class="w3-input w3-border" type="text" id="selectednumber" name="selectednumber">
                            <span class="error" style=""><?= $selectednoerror ?></span>
                            <br>
                            <span class="numericerror"><?= $selectednumerror ?></span>
                        </div>
                        <div class="w3-bar w3-third">
                            <br>
                            <button name="btnsubmit" class="w3-button w3-khaki">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

<?php
    // fizzbuzz statement //
    $selectednumber = $_POST['selectednumber'] ?? '';

    if (isset($_POST['btnsubmit'])) {
        for ($i = 1; $i <= ($selectednumber); $i++) { 
            if ($i%3 == 0 && $i%5 == 0) {
                echo "<h5 style='text-align: center;'>$i . FizzBuzz.</h5>";
            } elseif ($i%3 == 0) {
                echo "<h5 style='text-align: center;'>$i . Fizz.</h5>";
            } elseif ($i%5 == 0) {
                echo "<h5 style='text-align: center;'>$i . Buzz.</h5>";
            } else {
                echo "<h5 style='text-align: center;'>$i .</h5>";
            }
        }
    }
?>
</body>
</html>