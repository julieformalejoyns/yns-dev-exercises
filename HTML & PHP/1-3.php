<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
            .w3-sidebar {
            z-index: 3;
            width: 250px;
            top: 43px;
            bottom: 0;
            height: inherit;
        }

        .error, .numericerror {
            color: #FF0000;
        }
    </style>    
</head>
<body>
<?php
    // define variables and set to empty values
    $firstnoerror = "";
    $secondnoerror = "";
    $firstnumerror = "";
    $secondnumerror = "";
    $firstno = "";
    $secondno = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["firstno"])) {
            $firstnoerror = "Please fill up the first number";
        } else {
            $firstno = test_input($_POST["firstno"]);
        }

        if (empty($_POST["secondno"])) {
            $secondnoerror = "Please fill up the second number";
        } else {
            $secondno = test_input($_POST["secondno"]);
        }

        if (is_numeric($_POST["firstno"])) {
            $firstnumerror = "This is a valid number";
        } else {
            $firstno = test_input($_POST["firstno"]);
            $firstnumerror = "Invalid character. Numbers only";
        }

        if (is_numeric($_POST["secondno"])) {
            $secondnumerror = "This is a valid number";
        } else {
            $secondno = test_input($_POST["secondno"]);
            $secondnumerror = "Invalid character. Numbers only";
        }
    } 

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


?>
    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-3: Show the greatest common divisor.<h4>
                <br>
                <form method="post">
                    <div class="w3-row-padding">
                        <div class="w3-third">
                            <label>First Number</label>
                            <input class="w3-input w3-border" type="text" id="firstno" name="firstno">
                            <span class="error" style=""><?= $firstnoerror ?></span>
                            <br>
                            <span class="numericerror"><?= $firstnumerror ?></span>
                        </div>
                        <div class="w3-third">
                            <label>Second Number</label>
                            <input class="w3-input w3-border" type="text" id="secondno" name="secondno">
                            <span class="error"><?= $secondnoerror ?></span>
                            <br>
                            <span class="numericerror"><?= $secondnumerror ?></span>
                        </div>
                        <div class="w3-bar w3-third">
                            <br>
                            <button name="btnsubmit" class="w3-button w3-khaki">GET GCD</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

<?php
    // gcd statement //
    $firstno = $_POST['firstno'] ?? '';
    $secondno = $_POST['secondno'] ?? '';
    $gcd = 0;

    if ($firstno > $secondno) {
        $temp = $firstno;
        $firstno = $secondno;
        $secondno = $temp;
    }

    for ($i = 1; $i < ((int)$firstno+1); $i++) { 
        if ((int)$firstno%$i == 0 && (int)$secondno%$i == 0) 
            $gcd = $i;
    }
    echo "<h5 style='text-align: center;'>GCD of $firstno and $secondno is: $gcd</h5>";
?>
</body>
</html>