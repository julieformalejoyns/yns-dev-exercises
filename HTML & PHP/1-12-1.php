<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
            .w3-sidebar {
            z-index: 3;
            width: 250px;
            top: 43px;
            bottom: 0;
            height: inherit;
        }

        a:link, a:visited {
            background-color: #F0E68C;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: #C4A484;
        }
    </style>    
</head>
<body>
<?php
    // set session start //
    session_start();
    set_time_limit(0);

    // set session variables //
    $firstname = $_SESSION['firstname'];
    $middlename = $_SESSION['middlename'];
    $lastname = $_SESSION['lastname'];
    $birthdate = $_SESSION['birthdate'];
    $user_age = $_SESSION['user_age'];
    $email_address = $_SESSION['email_address'];
    $profilepicture = $_SESSION['profilepicture'] ?? '';

    $csvFile = fopen('UserInformation.csv', 'a');
    $userInput = array($firstname, $middlename, $lastname, $birthdate, $user_age, $email_address, $profilepicture);
    fputcsv($csvFile, $userInput);
    fclose($csvFile);

?>
    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-12-1: Preview User Information<h4>
                <form method="post" action="1-6-1.php">
                    <div class="w3-row-padding">
                        <h1 class="w3-text-teal">User Information</h1>
                        <div class="w3-third">
                            <label>First Name:</label>
                            <?= $_SESSION["firstname"] ?>
                            <br>
                            <label>Birth Date:</label>
                            <?= $_SESSION["birthdate"] ?>
                        </div>
                        <div class="w3-third">
                            <label>Middle Name:</label>
                            <?= $_SESSION["middlename"] ?>
                            <br>
                            <label>Age:</label>
                            <?= $_SESSION["user_age"] ?>
                        </div>
                        <div class="w3-third">
                            <label>Last Name:</label>
                            <?= $_SESSION["lastname"] ?>
                            <br>
                            <label>Email Address:</label>
                            <?= $_SESSION["email_address"] ?>
                        </div>
                        <div class="w3-third">
                            <label>Profile Picture:</label>
                            <br>
                            <?php if ($profilepicture): ?>
                                    <img src="<?= $profilepicture ?>" alt="profilepicture" width="300" height="300"><br/>
                            <?php endif ; ?>  
                            <br>
                            <a href="1-12-2.php">View Table Form</a>                            
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

</body>
</html>