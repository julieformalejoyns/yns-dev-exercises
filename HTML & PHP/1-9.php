<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
            .w3-sidebar {
            z-index: 3;
            width: 250px;
            top: 43px;
            bottom: 0;
            height: inherit;
        }

        .errorsfields {
            color: #FF0000;
        }
    </style>    
</head>
<body>
<?php
    session_start();
    set_time_limit(0);

    $firstname = $_POST['firstname'] ?? '';
    $middlename = $_POST['middlename'] ?? '';
    $lastname = $_POST['lastname'] ?? '';
    $birthdate = $_POST['birthdate'] ?? '';
    $user_age = $_POST['user_age'] ?? '';
    $email_address = $_POST['email_address'] ?? '';

    $errorsfields = array();
    $requiredfields = array('firstname','middlename','lastname','birthdate','user_age','email_address');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($requiredfields as $key => $input) {
            if (empty($_POST[$input])){
                array_push($errorsfields,ucwords($input) .' is required');
            }
        }
        if (!ctype_alpha($firstname) || !ctype_alpha($middlename) || !ctype_alpha($lastname)){
            array_push($errorsfields, 'Name should be consist of letters and white space allowed.');
        }
        if ($user_age && !is_numeric($user_age)){
            array_push($errorsfields, 'Age should only be in numeric value only.');
        }
        if ($email_address && !filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
            array_push($errorsfields, 'Email is in invalid format.');
        }
        if (!$errorsfields){
            $_SESSION['firstname'] = $firstname;
            $_SESSION['middlename'] = $middlename;
            $_SESSION['lastname'] = $lastname;
            $_SESSION['birthdate'] = $birthdate;
            $_SESSION['user_age'] = $user_age;
            $_SESSION['email_address'] = $email_address;

            header("location:1-9-1.php");
        }
    }
?>

    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-9: Show the user information using table tags.<h4>
                <form method="post" action="1-9.php">
                    <div class="w3-row-padding">
                        <h1 class="w3-text-teal">User Information</h1>
                        <div class="w3-third">
                            <label>First Name</label>
                            <input class="w3-input w3-border" type="text" id="firstname" name="firstname">
                            <br>
                            <label>Birth Date</label>
                            <input class="w3-input w3-border" type="date" id="birthdate" name="birthdate">
                        </div>
                        <div class="w3-third">
                            <label>Middle Name</label>
                            <input class="w3-input w3-border" type="text" id="middlename" name="middlename">
                            <br>
                            <label>Age</label>
                            <input class="w3-input w3-border" type="text" id="user_age" name="user_age">
                        </div>
                        <div class="w3-third">
                            <label>Last Name</label>
                            <input class="w3-input w3-border" type="text" id="lastname" name="lastname">
                            <br>
                            <label>Email Address</label>
                            <input class="w3-input w3-border" type="text" id="email_address" name="email_address">
                        </div>
                    </div>
                    <div class="w3-third w3-bar">
                        <br>
                        <button name="btnsubmit" class="w3-button w3-khaki" style="margin-left: 15px;">Submit</button>
                    </div>
                    <div class="w3-third">
                        <h5 class="errorsfields">
                            <?php
                                if ($errorsfields) {
                                    foreach ($errorsfields as $key => $errorsfield) {
                                        echo $errorsfield.'<br/>';
                                    }
                                }
                            ?>
                        </h5>
                    </div>
                </form>
            </div>
        </div>

    </div>
</body>
</html>