<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        #userinformation {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #userinformation td, #userinformation th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #userinformation tr:nth-child(even){
            background-color: #f2f2f2;
        }

        #userinformation tr:hover {
            background-color: #ddd;
        }

        #userinformation th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>    
</head>
<body>
<?php
    $usertable = array();
    $csvFile = fopen('./UserInformation.csv', 'r');
    while (($csvData = fgetcsv($csvFile)) !== false) {
        array_push($usertable,$csvData);   
    }
    fclose($csvFile);
    $userpage = $_GET['userpage'] ?? 1;
    $limitentries = 10;
    $total = count($usertable);
    $offset = ($userpage - 1) * $limitentries;
    $pagenumber = ceil($total / $limitentries);
    $results = array_slice($usertable,$offset,$limitentries);
?>
    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-12-2: Preview User Information (Table Form) .<h4>
                <form method="post" action="1-12.php">
                    <div class="w3-row-padding">
                        <h1 class="w3-text-teal">User Information</h1>
                        <table id="userinformation">
                            <tr>
                                <td>First Name</td>
                                <td>Middle Name</td>
                                <td>Last Name</td>
                                <td>Birth Date</td>
                                <td>Age</td>
                                <td>Email Address</td>
                                <td>Profile Picture</td>
                            </tr>
                            <tr>
                            <?php
                                $csvFile = fopen('./UserInformation.csv', 'r');
                                while (($csvData = fgetcsv($csvFile)) !== false) {
                                    echo '<tr>';
                                    for ($i=0; $i < 7 ; $i++) { 
                                        if ($i!==6){
                                            echo '<td>' .$csvData[$i]. '</td>';
                                        } else if (!empty($csvData[6])) {
                                            echo "<td><img src=".$csvData[6]." height=200 width=200 /></td>";
                                        }
                                    }
                                    echo "</tr>\n";
                                }
                                fclose($csvFile);
                            ?>
                            </tr>
                        </table>
                    </div>
                    <h5>
                    <?php
                        for ($i=1; $i <= $pagenumber ; $i++) { 
                            echo '<a href="1-12-2.php?page='.($i).'">'.$i.'</a>';
                        }
                    ?>
                    </h5>
                </form>
            </div>
        </div>

    </div>
</body>
</html>