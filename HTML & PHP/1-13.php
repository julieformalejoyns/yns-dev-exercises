<!DOCTYPE html>
<html>
<head>
    <title>Phase One</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
            .w3-sidebar {
            z-index: 3;
            width: 250px;
            top: 43px;
            bottom: 0;
            height: inherit;
        }

        .errorsfields {
            color: #FF0000;
        }
    </style>    
</head>
<body>
<?php
    session_start();
    set_time_limit(0);

    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    $errorsfields = array();
    $requiredfields = array('username','password');
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($requiredfields as $key => $input) {
            if (empty($_POST[$input])){
                array_push($errorsfields,ucwords($input) .' is required');
            }
        }
        if (!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)) {
           array_push($requirements, 'Username should only contains alpha numeric characters');
        } 
        if (!$errorsfields){
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;

            header("location:1-13-1.php");
        }
    }
?>

    <div class="w3-top">
        <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
            <a href="#" class="w3-bar-item w3-button w3-theme-l1">YNS</a>
        </div>
    </div>

    <div class="w3-main">
        
        <div class="w3-row w3-padding-64">
            <div class="w3-container">
                <h1 class="w3-text-teal">HTML & PHP</h1>
                <h4 class="w3-text-black">Exercise Problem 1-13: Create login form and embed it into the system that you developed.<h4>
                <form method="post" action="1-13.php">
                    <div class="w3-row-padding">
                        <h1 class="w3-text-teal">Login Account</h1>
                        <div class="w3-third">
                            <label>Username</label>
                            <input class="w3-input w3-border" type="text" id="username" name="username">
                            <br>
                            <label>Password</label>
                            <input class="w3-input w3-border" type="password" id="password" name="password">
                            <br>
                            <button name="btnsubmit" class="w3-button w3-khaki" style="margin-left: 5px;">Login</button>
                        </div>
                    </div>
                    <div class="w3-third">
                        <h5 class="errorsfields">
                            <?php
                                if ($errorsfields) {
                                    foreach ($errorsfields as $key => $errorsfield) {
                                        echo $errorsfield.'<br/>';
                                    }
                                }
                            ?>
                        </h5>
                    </div>
                </form>
            </div>
        </div>

    </div>
</body>
</html>