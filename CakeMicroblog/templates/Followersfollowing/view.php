<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Followersfollowing $followersfollowing
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Followersfollowing'), ['action' => 'edit', $followersfollowing->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Followersfollowing'), ['action' => 'delete', $followersfollowing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $followersfollowing->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Followersfollowing'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Followersfollowing'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="followersfollowing view content">
            <h3><?= h($followersfollowing->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $followersfollowing->has('user') ? $this->Html->link($followersfollowing->user->id, ['controller' => 'Users', 'action' => 'view', $followersfollowing->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($followersfollowing->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Follow Id') ?></th>
                    <td><?= $this->Number->format($followersfollowing->follow_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Following Id') ?></th>
                    <td><?= $this->Number->format($followersfollowing->following_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Datetime') ?></th>
                    <td><?= h($followersfollowing->created_datetime) ?></td>
                </tr>
                <tr>
                    <th><?= __('Deleted Datetime') ?></th>
                    <td><?= h($followersfollowing->deleted_datetime) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Deleted') ?></th>
                    <td><?= $followersfollowing->is_deleted ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
