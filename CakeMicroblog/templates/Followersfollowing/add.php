<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Followersfollowing $followersfollowing
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Followersfollowing'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="followersfollowing form content">
            <?= $this->Form->create($followersfollowing) ?>
            <fieldset>
                <legend><?= __('Add Followersfollowing') ?></legend>
                <?php
                    echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
                    echo $this->Form->control('follow_id');
                    echo $this->Form->control('following_id');
                    echo $this->Form->control('is_deleted');
                    echo $this->Form->control('created_datetime', ['empty' => true]);
                    echo $this->Form->control('deleted_datetime', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
