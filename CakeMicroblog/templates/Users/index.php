<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<style>
    /* Pagination links */
    .pagination {
        padding-top: 5px;
    }
    .pagination a {
        color: #db0c34;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination li.active a {
        background-color: #db0c34;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}
</style>
<section id="search">
        <div class="feed-header">
        <h2>Home</h2>
    </div>
<body  style="background-color: #EDE9E8;">

<div class="container">
    <div class="well well-lg" style="height:100%; margin-top:20px; margin-right: 5px;">
        <div class="row">
            <div class="col-lg-3">
                <div class="list-group-item">
                    <section class="sidebar">
                        <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-home" style="color: #cf352e;"></i>&nbsp;Home</h4>
                        </a>
                        <a href="/explore" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-hashtag" style="color: #cf352e;"></i>&nbsp;Explore</h4>
                        </a>
                        <a href="#" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-bell" style="color: #cf352e;"></i>&nbsp;Notifications</h4>
                        </a>
                        <a href="profile" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-user" style="color: #cf352e;"></i>&nbsp;&nbsp;Profile</h4>
                        </a>
                        <a href="/users/logout" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-sign-out" style="color: #cf352e;"></i>&nbsp;Sign Out</h4>
                        </a>
                        <button id="myBtn" onclick="openModal();">Post</button>
                    </section>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
            <div class="col-lg-6">
                    <div class="paginator">
                        <ul class="pagination"  style="padding-bottom: 0px !important; margin: 0px !important;">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers(array('modulus' => 4)) ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <small style="padding-left: 15px;"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></small>
                    </div>
            </div>
            <div class="col-lg-3">
                <div class="list-group-item">
                    <section class="sidebar">
                        <h3>Trends for You</h3>
                        <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-home" style="color: #cf352e;"></i>&nbsp;Home</h4>
                        </a>
                        <a href="/explore" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-hashtag" style="color: #cf352e;"></i>&nbsp;Explore</h4>
                        </a>
                        <a href="#" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-bell" style="color: #cf352e;"></i>&nbsp;Notifications</h4>
                        </a>
                        <a href="/users/profile" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-user" style="color: #cf352e;"></i>&nbsp;&nbsp;Profile</h4>
                        </a>
                        <a href="/users/logout" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-sign-out" style="color: #cf352e;"></i>&nbsp;Sign Out</h4>
                        </a>
                </div>
                <br>
                <div class="list-group-item">
                    <section class="sidebar">
                        <h3>Who to Follow</h3>
                        <a href="/" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-home" style="color: #cf352e;"></i>&nbsp;Home</h4>
                        </a>
                        <a href="/explore" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-hashtag" style="color: #cf352e;"></i>&nbsp;Explore</h4>
                        </a>
                        <a href="#" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-bell" style="color: #cf352e;"></i>&nbsp;Notifications</h4>
                        </a>
                        <a href="/users/profile" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-user" style="color: #cf352e;"></i>&nbsp;&nbsp;Profile</h4>
                        </a>
                        <a href="/users/logout" class="sidebar-option" style="text-decoration: none; color: #cf352e;">
                            <h4 style="font-family: Arial;"><i class="fa fa-sign-out" style="color: #cf352e;"></i>&nbsp;Sign Out</h4>
                        </a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<div id="NewPostModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #cf352e;">New Post</h4> 
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control input-md input-flat" id="newPost" rows="4" autocomplete="off" maxlength="140"></textarea>
                                    <span id="count">Count :</span>
                                    <div class="image-upload">
                                        <label for="file-input">
                                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                                        </label>
                                            <input id="file-input" type="file"/>
                                    </div>
                                    <div class="gallery" style="width: 350px;"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Post', array('type' => 'button', 'class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" onclick="closemodal();"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div id="EditPostModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close">&times;</button>
                <h4 class="modal-title" style="color: #cf352e;">Edit Post</h4> 
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <input type="hidden" name="posts_id">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Retweet Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control input-md input-flat" id="updatePost" rows="4" autocomplete="off" maxlength="140" placeholder="add mment"></textarea>
                                    <span id="updatecount">Count :</span>
                                    <div class="image-upload">
                                        <label for="file-input">
                                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                                        </label>
                                            <input id="updatefile-input" type="file"/>
                                    </div>
                                    <div class="updategallery" style="width: 350px;"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Post', array('type' => 'button', 'class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" onclick="closemodal();"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div id="RetweetPostModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close">&times;</button>
                <h4 class="modal-title" style="color: #cf352e;">Retweet Post</h4> 
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <input type="hidden" name="posts_id">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control input-md input-flat" id="updatePost" rows="4" autocomplete="off" maxlength="140"></textarea>
                                    <span id="updatecount">Count :</span>
                                    <div class="image-upload">
                                        <label for="file-input">
                                            <i class="attach-doc fa fa-image fa-1x" aria-hidden="true" style="color: #cf352e;"></i>
                                        </label>
                                            <input id="updatefile-input" type="file"/>
                                    </div>
                                    <div class="updategallery" style="width: 350px;"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Post', array('type' => 'button', 'class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" onclick="closemodal();"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="modal" id="ViewPostModal" tabindex="-1" role="dialog" aria-labelledby="vsharePostModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="vsharePostModalLabel">View Full Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body vsharePostModalBody">
        <div class="row formvsPostInputs">
        <input type="hidden" name="vpost_id">
          <!-- Post -->
          <div class="col-md-12" id="vcontent">
          </div>
          <!-- Post -->
        </div>
        <div style="padding-top:20px;"></div>
        <div id="vorginal_post_content" style="border-radius: 0.25rem; border: 1px solid #ced4da; padding: 10px 20px 10px;">
            <small><i class="mdi mdi-share"></i> Re-posted from</small>
            <br/>
            <div style="display: inline-block;">
                <div style="display: inline-block;">
                    <img src="" alt="Image" class="img-fluid" style="width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;" id="vshare_post_pic">
                </div>
                <div style="padding-left: 10px; font-size: 12px; display: inline-block;">
                    <b id="vshare_post_name" style="display: inline-block"></b>
                    <div id="vshare_post_user_date" style="display: inline-block"></div>
                </div>
                <br/>
            </div>
            <div style="padding-top:10px;"></div>
            <div id="vshare_post_content" style="font-size:13px;"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id="DeletePostModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #cf352e;">Delete Post</h4> 
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <input type="hidden" name="posts_id">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>This can't be undone and it will be removed from your profile and the timeline of any accounts that follow you.</h3>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Delete', array('type' => 'button', 'class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" onclick="closemodal();"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>


<div id="DeletePostModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: #cf352e;">Unfollow User</h4> 
            </div>
            <?= $this->Form->create()?>
            <div class="modal-body">
                <input type="hidden" name="posts_id">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset style="margin: 2px;border: 1px dotted #CCC;padding: 8px;margin-top: 0px;width: 100%;">
                        <legend style="border: none;margin-bottom: 0px;font-size: 15px; font-weight: normal;width:110px;">&nbsp;&nbsp;Post Details &nbsp;&nbsp;</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Unfolow?</h2>
                                    <h3>This can't be undone and it will be removed from your profile and the timeline of any accounts that follow you.</h3>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 10px; padding-top: 10px;"> 
                <?= $this->Form->button('Delete', array('type' => 'button', 'class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
                <button class="btn btn-danger bt-sm" onclick="closemodal();"> Close</button>                      
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>



<script type="text/javascript">
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#file-input').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});  

  function openModal() {
    $('#NewPostModal').modal('show');
  }

    function closemodal() {
        var isValid = 1;
        if ($('#newPost').val() == "" && $('#gallery').html() == "") {
            isValid = 0;
            $('#NewPostModal').modal('hide');
        } else if ($('#newPost').val() || $('#gallery').html()) {
            Swal.fire({
                title: "Confirm",
                text: "Are you sure you want to discard this data?",
                type: "question",
                showCancelButton: true,
                confirmButtonColor: "#009efb",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                allowOutsideClick: false                
            }).then((result) => {
                if (result.value) {
                    $('#NewPostModal').modal('hide');
                }
            })
        } else {
            $('#NewPostModal').modal('hide');
        }
        return isValid;
    }

    function closeeditmodal() {
        var isValid = 1;
        if ($('#updatePost').val() || $('#updategallery').html()) {
            Swal.fire({
                title: "Confirm",
                text: "Are you sure you want to exit?",
                type: "question",
                showCancelButton: true,
                confirmButtonColor: "#009efb",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                allowOutsideClick: false                
            }).then((result) => {
                if (result.value) {
                    $('#EditPostModal').modal('hide');
                } else if (result.dismiss == 'cancel') {
                    $('#EditPostModal').modal('show');
                }
            })
        } else {
            $('#EditPostModal').modal('hide');
        }
        return isValid;
    }


  var a = document.getElementById("newPost");
    a.addEventListener("keyup",function(){
    document.getElementById("count").innerHTML = "Count :" + " "+ a.value.length;
  })

  var b = document.getElementById("updatePost");
    b.addEventListener("keyup",function(){
    document.getElementById("updatecount").innerHTML = "Count :" + " "+ a.value.length;
  })    
</script>

<style type="text/css">
.image-upload > input
{
    display: none;
}
.attach-doc
{
    cursor: pointer;
}
</style>
</section>
