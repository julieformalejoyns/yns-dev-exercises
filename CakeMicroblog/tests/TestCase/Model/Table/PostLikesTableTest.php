<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostLikesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostLikesTable Test Case
 */
class PostLikesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PostLikesTable
     */
    protected $PostLikes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PostLikes',
        'app.Users',
        'app.Posts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PostLikes') ? [] : ['className' => PostLikesTable::class];
        $this->PostLikes = $this->getTableLocator()->get('PostLikes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PostLikes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PostLikesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PostLikesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
