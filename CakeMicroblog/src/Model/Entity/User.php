<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $email
 * @property string|null $username
 * @property string|null $password
 * @property string|null $profile_image
 * @property bool|null $is_activated
 * @property bool|null $is_verified
 * @property bool|null $is_private
 * @property string|null $password_hash
 * @property \Cake\I18n\FrozenTime|null $created_datetime
 * @property \Cake\I18n\FrozenTime|null $modified_datetime
 * @property \Cake\I18n\FrozenTime|null $deleted_datetime
 *
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\FollowersFollowing[] $followers_following
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\PostLike[] $post_likes
 * @property \App\Model\Entity\Post[] $posts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'email' => true,
        'username' => true,
        'password' => true,
        'profile_image' => true,
        'is_activated' => true,
        'is_verified' => true,
        'is_private' => true,
        'password_hash' => true,
        'created_datetime' => true,
        'modified_datetime' => true,
        'deleted_datetime' => true,
        'comments' => true,
        'followers_following' => true,
        'notifications' => true,
        'post_likes' => true,
        'posts' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
}
