<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notification Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $follow_id
 * @property int|null $posts_id
 * @property string|null $content
 * @property \Cake\I18n\FrozenTime|null $date_created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Follow $follow
 * @property \App\Model\Entity\Post $post
 */
class Notification extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'follow_id' => true,
        'posts_id' => true,
        'content' => true,
        'date_created' => true,
        'user' => true,
        'follow' => true,
        'post' => true,
    ];
}
