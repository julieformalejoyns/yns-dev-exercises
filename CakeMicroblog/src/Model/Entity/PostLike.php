<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PostLike Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $posts_id
 * @property bool|null $is_deleted
 * @property \Cake\I18n\FrozenTime|null $created_datetime
 * @property \Cake\I18n\FrozenTime|null $deleted_datetime
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Post $post
 */
class PostLike extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'posts_id' => true,
        'is_deleted' => true,
        'created_datetime' => true,
        'deleted_datetime' => true,
        'user' => true,
        'post' => true,
    ];
}
