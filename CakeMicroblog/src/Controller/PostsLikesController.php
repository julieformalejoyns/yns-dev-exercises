<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * PostsLikes Controller
 *
 * @method \App\Model\Entity\PostsLike[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsLikesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $postsLikes = $this->paginate($this->PostsLikes);

        $this->set(compact('postsLikes'));
    }

    /**
     * View method
     *
     * @param string|null $id Posts Like id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $postsLike = $this->PostsLikes->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('postsLike'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $postsLike = $this->PostsLikes->newEmptyEntity();
        if ($this->request->is('post')) {
            $postsLike = $this->PostsLikes->patchEntity($postsLike, $this->request->getData());
            if ($this->PostsLikes->save($postsLike)) {
                $this->Flash->success(__('The posts like has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posts like could not be saved. Please, try again.'));
        }
        $this->set(compact('postsLike'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Posts Like id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $postsLike = $this->PostsLikes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $postsLike = $this->PostsLikes->patchEntity($postsLike, $this->request->getData());
            if ($this->PostsLikes->save($postsLike)) {
                $this->Flash->success(__('The posts like has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posts like could not be saved. Please, try again.'));
        }
        $this->set(compact('postsLike'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Posts Like id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $postsLike = $this->PostsLikes->get($id);
        if ($this->PostsLikes->delete($postsLike)) {
            $this->Flash->success(__('The posts like has been deleted.'));
        } else {
            $this->Flash->error(__('The posts like could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
