<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * FollowersFollowing Controller
 *
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersFollowingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $followersFollowing = $this->paginate($this->FollowersFollowing);

        $this->set(compact('followersFollowing'));
    }

    /**
     * View method
     *
     * @param string|null $id Followers Following id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $followersFollowing = $this->FollowersFollowing->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('followersFollowing'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $followersFollowing = $this->FollowersFollowing->newEmptyEntity();
        if ($this->request->is('post')) {
            $followersFollowing = $this->FollowersFollowing->patchEntity($followersFollowing, $this->request->getData());
            if ($this->FollowersFollowing->save($followersFollowing)) {
                $this->Flash->success(__('The followers following has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The followers following could not be saved. Please, try again.'));
        }
        $this->set(compact('followersFollowing'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Followers Following id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $followersFollowing = $this->FollowersFollowing->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $followersFollowing = $this->FollowersFollowing->patchEntity($followersFollowing, $this->request->getData());
            if ($this->FollowersFollowing->save($followersFollowing)) {
                $this->Flash->success(__('The followers following has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The followers following could not be saved. Please, try again.'));
        }
        $this->set(compact('followersFollowing'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Followers Following id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $followersFollowing = $this->FollowersFollowing->get($id);
        if ($this->FollowersFollowing->delete($followersFollowing)) {
            $this->Flash->success(__('The followers following has been deleted.'));
        } else {
            $this->Flash->error(__('The followers following could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
